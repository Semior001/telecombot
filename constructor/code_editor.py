# _*_ coding: utf-8 _*_

"""
Created on 24.08.2016

:author: Semior001
Модуль для запуска редактора кода
"""

import traceback
import sys

from PyQt4 import QtCore
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *
import configparser

app = QApplication(sys.argv)

# Открываем файл конфига и задаем все начальные значения
preferences = configparser.ConfigParser()
preferences.read("preferences.cfg")

# Создаем объект окна редактора кода
win = QWidget()
win.setWindowTitle('Code Editor')

# Создаем и задаем шаблоны редактора кода
main_layout = QHBoxLayout()
win.setLayout(main_layout)
sidebar_layout = QVBoxLayout()
font_size_layout = QHBoxLayout()


class WebPage(QWebPage):
    """
    Класс веб-страницы для отладки js
    """

    def __init__(self, url, parent=None):
        """
        Инициализирует класс веб-страницы
        :param url: адрес веб-страницы
        :param parent: предок данной страницы
        """
        QWebPage.__init__(self, parent)
        self.mainFrame().load(QtCore.QUrl(url))

    def javaScriptConsoleMessage(self, msg, line, source):
        """
        Выводит сообщение из js консоли в stdout
        :param msg:
        :param line:
        :param source:
        """
        print('%s line %d: %s' % (source, line, msg))


# Создаем экземпляр веб-страниы
editor_page = WebPage("ace_editor/editor.html")

# Создаем объект браузера - основной объект редактора кода
view = QWebView()

# Задаем браузеру страницу
view.setPage(editor_page)

# Создаем и заполняем выпадающий список с темами редактора кода
select_theme = QComboBox()
select_theme.addItem("Monokai")
select_theme.addItem("Xcode")
select_theme.addItem("Cobalt")
select_theme.addItem("Eclipse")
select_theme.addItem("Vibrant ink")

# Создаем и заполняем поле с размером шрифта редактора кода
font_size_edit = QLineEdit()
font_size_edit.setText("16")

# Добавляем в горизонтальный макет объекты смены шрифта
# (текстовое поле и кнопку)
font_size_layout.addWidget(font_size_edit)

# Labels
select_theme_label = QLabel()
select_theme_label.setText("Change theme")

font_size_label = QLabel()
font_size_label.setText("Change font size")

# Создаем объект кнопки, которая откроет окно с справкой
open_help_btn = QPushButton("Help")

# Создаем объект кнопки, которая выполнит написанный код
save_btn = QPushButton("Save")


def select_theme_func(select):
    """
    Функция, которая вызывается при смене темы
    Задает тему редактора
    :param select: номер выбранной темы
    """
    if select == 0:
        theme = "monokai"
    elif select == 1:
        theme = "xcode"
    elif select == 2:
        theme = "cobalt"
    elif select == 3:
        theme = "eclipse"
    elif select == 4:
        theme = "vibrant_ink"
    frame = editor_page.mainFrame()
    frame.evaluateJavaScript('editor.setTheme("ace/theme/' + theme + '");')
    if not preferences.has_section("Code editor"):
        preferences.add_section("Code editor")
    preferences.set("Code editor", "theme", str(select))
    with open("preferences.cfg", "w") as configfile:
        preferences.write(configfile)


def on_font_size_changed():
    """
    Функция, которая вызывается при смене размера шрифта
    Задает размер шрифта редактора
    """
    frame = editor_page.mainFrame()
    font_size = font_size_edit.text()
    frame.evaluateJavaScript("document.getElementById('editor').style.fontSize='" + font_size + "px';")
    if not preferences.has_section("Code editor"):
        preferences.add_section("Code editor")
    preferences.set("Code editor", "font_size", font_size)
    with open("preferences.cfg", "w") as configfile:
        preferences.write(configfile)


def save_btn_func():
    """
    Добавляет написанную функцию в поле on_click текущей кнопки
    """

    # Получаем текст из редактора
    frame = editor_page.mainFrame()
    code = frame.evaluateJavaScript("editor.getValue()")

    try:
        # Выполняем текст из редактора как python-код
        exec(code, globals())
        # Теперь для присваивания надо использовать имя on_click
        # TODO: Сделать привязку текущего on_click с кнопкой, для которой вызвали редактор
    except:
        # Если написанный код - херня, то выводим msgbox с текстом ошибки
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Compilation error")
        msg.setWindowTitle("Compilation error")
        error = traceback.format_exc().splitlines()
        formatted_error = error[3][2:] + "\n"
        for line in error[4:]:
            formatted_error += "    " + line + "\n"
        msg.setDetailedText("Details:\n" + formatted_error)
        msg.setStandardButtons(QMessageBox.Close)
        msg.findChild(QTextEdit).setFixedSize(400, 300)
        msg.exec()


def open_help_func():
    """
    Функция, которая открывает страницу помощи
    """
    import webbrowser
    webbrowser.open('ace_editor/help_page.html')


def whenLoadFinished(ok):
    """
    Функция для загрузки параметров, вроде темы редактора и размера шрифта
    :param ok:
    :return:
    """
    # Задаем начальные значения редактора
    preferences.read("preferences.cfg")

    # Загружаем тему
    frame = editor_page.mainFrame()
    if preferences.has_section("Code editor"):
        select = preferences.getint("Code editor", "theme")
        if select == 0:
            theme = "monokai"
        elif select == 1:
            theme = "xcode"
        elif select == 2:
            theme = "cobalt"
        elif select == 3:
            theme = "eclipse"
        elif select == 4:
            theme = "vibrant_ink"
        frame.evaluateJavaScript('editor.setTheme("ace/theme/' + theme + '");')
        select_theme.setCurrentIndex(select)

        # Загружаем размер шрифта
        font_size = preferences.get("Code editor", "font_size")
        frame.evaluateJavaScript("document.getElementById('editor').style.fontSize='" + font_size + "px';")
        font_size_edit.setText(font_size)
    else:
        preferences.add_section("Code editor")
        preferences.set("Code editor", "theme", "0")
        preferences.set("Code editor", "font_size", "16")
        with open("preferences.cfg", "w") as configfile:
            preferences.write(configfile)


# Связываем все кнопки с их функциями
select_theme.currentIndexChanged.connect(select_theme_func)
font_size_edit.textChanged.connect(on_font_size_changed)
save_btn.clicked.connect(save_btn_func)
open_help_btn.clicked.connect(open_help_func)

# Связываем событие загрузки страницы с функцией загрузки параметров
editor_page.mainFrame().loadFinished.connect(whenLoadFinished)

# Заполняем sidebar
sidebar_layout.addWidget(select_theme_label)
sidebar_layout.addWidget(select_theme)
sidebar_layout.addWidget(font_size_label)
sidebar_layout.addLayout(font_size_layout)
sidebar_layout.addStretch(1)
sidebar_layout.addWidget(open_help_btn)
sidebar_layout.addWidget(save_btn)

# Добавляем в главный макет браузер и sidebar
main_layout.addWidget(view)
main_layout.addLayout(sidebar_layout)

# Задаем размеры редактора
win.resize(1000, 600)
win.show()
sys.exit(app.exec_())
