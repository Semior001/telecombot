var langTools = ace.require("ace/ext/language_tools");
var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.session.setMode("ace/mode/python");
document.getElementById('editor').style.fontSize='16px';
editor.$blockScrolling = Infinity;
editor.moveCursorTo(7, 8); // Set cursor position after last char
editor.setOptions({
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: false
});