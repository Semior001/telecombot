# _*_ coding: utf-8 _*_

"""
Created on 26.07.2016

:author: Semior001
Модуль для конструирования архитектуры бота
"""

from PyQt4 import QtCore
from PyQt4 import QtGui
import sys
import gearbot3


def click():
    import code_editor
    code_editor.win.show()


class MyWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.label = QtGui.QLabel("Привет, мир!")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.btn = QtGui.QPushButton("&Открыть редактор кода")
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.btn)
        self.setLayout(self.vbox)
        self.btn.clicked.connect(click)


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    window = MyWindow()
    # Создаем экземпляр класса
    window.setWindowTitle("Constructor")
    window.resize(300, 70)
    window.show()
    sys.exit(app.exec_())
