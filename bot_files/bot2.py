# _*_ coding: utf-8 _*_

"""
Created on 26.07.2016

:author: Semior001
Модуль для запуска telegram-бота
"""

import shelve

import telebot

import constants, botframe2, gearbot3

gearbot3.open_intent()
gearbot3.open_checkboxes()

storage = shelve.open("Storage/shelve.db", writeback=True)

try:
    tb = telebot.TeleBot(constants.token)


    @tb.message_handler(commands=['help'])
    def handle_help(message):
        tb.send_message(message.chat.id, '''/start - Начать взаимодействие
    /debug - debug mode, only for developers
    /reload - reload botframe2, gearbot3 modules, only for developers
    /help - вывести список команд''')


    @tb.message_handler(commands=['start'])
    def handle_start(message):
        storage[str(message.chat.id)] = botframe2.root
        tb.send_message(message.chat.id, '''Здравствуйте!
Вас приветствует Telegram-бот АО "Казахтелеком"!''')
        botframe2.root.on_click(tb, message.chat.id, message.text)
        gearbot3.intents_storage[str(message.chat.id)] = {}
        gearbot3.checkboxes_storage[str(message.chat.id)] = {}


    @tb.message_handler(commands=['debug'])
    def handle_start(message):
        # Выводит в консоль весь путь пользователя
        print()
        print("------------------------------------")
        section_names = []
        i = storage[str(message.chat.id)]
        while i.parent is not None:
            section_names.append(i.text)
            i = i.parent
        section_names.append("Root")
        section_names.reverse()
        for i in section_names:
            print(i)
        print("------------------------------------")
        print()


    @tb.message_handler(content_types=['text'])
    def handle_text(message):
        if message.text == "Назад" and storage[str(message.chat.id)] != botframe2.root:
            storage[str(message.chat.id)] = storage[str(message.chat.id)].parent
            storage[str(message.chat.id)].on_click(tb, message.chat.id, message.text)

        elif message.text == "На главную":
            storage[str(message.chat.id)] = botframe2.root
            storage[str(message.chat.id)].on_click(tb, message.chat.id, message.text)

        elif issubclass(type(storage[str(message.chat.id)]), gearbot3.Intent):

            have_button = False

            for i in storage[str(message.chat.id)].sub_buttons:
                if i.text == message.text:
                    have_button = True
                    # Если кнопка - секция или объект, схожий по типу с секцией
                    if issubclass(type(i), gearbot3.Section):
                        storage[str(message.chat.id)] = i
                        storage[str(message.chat.id)].on_click(tb, message.chat.id, message.text)
                        break
                    # Но если это действительно кнопка или объкет, схожий по типу с кнопкой
                    elif issubclass(type(i), gearbot3.Button):
                        i.on_click(tb, message.chat.id, message.text)
                        break

            if not have_button:
                # Если это данные для ввода
                outtext = storage[str(message.chat.id)].writed(tb, message.chat.id, message.text)

                if outtext == 0:
                    # Если это секция и надо перейти туда
                    storage[str(message.chat.id)] = storage[str(message.chat.id)].final
                    storage[str(message.chat.id)].on_click(tb, message.chat.id, message.text)

        elif issubclass(type(storage[str(message.chat.id)]), gearbot3.Section):

            have_button = False

            for i in storage[str(message.chat.id)].sub_buttons:
                if i.text == message.text:
                    have_button = True
                    # Если кнопка - секция или объект, схожий по типу с секцией (Intent, к примеру)
                    if issubclass(type(i), gearbot3.Section):
                        storage[str(message.chat.id)] = i
                        storage[str(message.chat.id)].on_click(tb, message.chat.id, message.text)
                        break
                    # Но если это действительно кнопка или объкет, схожий по типу с кнопкой
                    elif issubclass(type(i), gearbot3.Button):
                        i.on_click(tb, message.chat.id, message.text)
                        break

                if issubclass(type(i), gearbot3.CheckBox) and i.text == message.text[:-2]:
                    have_button = True
                    i.on_click(tb, message.chat.id, message.text)
                    break

            if not have_button:
                tb.send_message(message.chat.id, "Неизвестная команда")


    tb.polling()

finally:
    storage.close()
    print("shelve.db закрыт")
    gearbot3.close_intent()
    print("intents.db закрыт")
    gearbot3.close_checkboxes()
    print("checkboxes.db закрыт")
