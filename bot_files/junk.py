# _*_ coding: utf-8 _*_

"""
Created on 16.08.2016

:author: Semior001
Модуль, который содержит не используемые ныне технологии
"""

import shelve

import telebot
from telegram import ParseMode
from gearbot3 import *


class CheckBoxGroup(Section):
    """ Класс группы checkbox """

    def __init__(self, text, parent, outText="Выберите:", need_cancel=2, row=-1, this_id=-1):
        """
        Инициализирует класс CheckBoxGroup
        """
        Section.__init__(self, text, parent, outText, need_cancel, row)
        if this_id == -1:
            raise TypeError("__init__() missing 1 required positional argument: 'this_id'")
        self.id = this_id

    def get_keyboard(self):
        """
        Формирует клавиатуру для отправки пользователю
        :return: клавиатура для отправки пользователю (telebot.types.ReplyKeyboardMarkup)
        """
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)

        for i in self.sub_buttons:
            if issubclass(type(i), CheckBox):
                if i.checked:
                    markup.row(telebot.types.KeyboardButton(i.text + " ✔"))
                else:
                    markup.row(telebot.types.KeyboardButton(i.text + "  "))
            else:
                markup.row(i.get_button())

        if self.need_cancel == 1:
            markup.row(telebot.types.KeyboardButton('Назад'))
        elif self.need_cancel == 2:
            markup.row(telebot.types.KeyboardButton('Назад'), telebot.types.KeyboardButton('На главную'))

        return markup

    def on_subbutton_click(self, tb, chat_id, message_text):
        """
        Вызывается при нажатии на кнопку внутри этой группы
        :param tb: объект сессии с telegram ботом
        :param chat_id: id обрабатываемого чата
        :param message_text: текст принятого сообщения
        """
        tb.send_message(chat_id, self.out_text, reply_markup=self.get_keyboard(), parse_mode=ParseMode.MARKDOWN)


class CheckBox(Button):
    """ Класс флажка """

    def __init__(self, text, parent, default_check=False, row=-1):
        """
        Инициализирует класс CheckBox
        :type parent: CheckBoxGroup
        :param default_check: отмечает, выбран ли данный чекбокс по умолчанию
        """
        Button.__init__(self, text, parent, self.on_click, row)
        self.checked = default_check

    def on_click(self, tb, chat_id, message_text):
        """
        Функция, вызываемая при нажатии на кнопку
        :param tb: объект сессии с telegram ботом
        :param chat_id: id обрабатываемого чата
        :param message_text: текст принятого сообщения
        """
        self.checked = not self.checked
        if self.text in checkboxes_storage[chat_id][self.parent.this_id]:
            checkboxes_storage[str(chat_id)][self.parent.this_id].remove(self.text)
        else:
            checkboxes_storage[str(chat_id)][self.parent.this_id].add(self.text)
        self.parent.on_subbutton_click(tb, chat_id, message_text)
