# _*_ coding: utf-8 _*_

"""
Created on 26.07.2016

:author: Semior001
Модуль для тестирования объектов из модуля gearbot3
"""

import requests

import gearbot3

root = gearbot3.Section(text="Корневая секция",
                        out_text='''Выберите одну из следующих категорий для предоставления услуги:''')


def check_balance_func(tb, chat_id, message_text):
    gearbot3.intents_storage[str(chat_id)][0] = message_text
    with requests.Session() as s:
        hdrs = {
            'Host': 'telecom.kz',
            'Connection': 'keep-alive',
            'Content-Length': '0',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Origin': 'http://telecom.kz',
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'http://telecom.kz/',
            'Accept-Encoding': 'gzip, deflate',
            'Accept_Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
        }
        s.headers.update(hdrs)
        p = s.post('https://telecom.kz/ajax/pay/phonedata', cookies=s.cookies, headers=s.headers,
                   data={'phone': gearbot3.intents_storage[str(chat_id)][0]})
        try:
            if p.json()['success']:
                tb.send_message(chat_id, '''Сумма оплаты в тенге: ''' + str(p.json()['balance']))
                return 0
            else:
                tb.send_message(chat_id, '''Такого номера не существует''')
                return 1
        except:
            print()
            print("ОШИБКА")
            print("Данные: " + gearbot3.intents_storage[str(chat_id)][0])
            print("Результат" + p.text)
            print()
            tb.send_message(chat_id, '''Произошла непредвиденная ошибка''')
            return 0


def feedback_func(tb, chat_id, message_text):
    pass


# "/"
shares_section = gearbot3.Section('Акции', root, need_cancel=1, row=0)
packages_section = gearbot3.Section('Пакеты', root, need_cancel=1, row=0)
tv_section = gearbot3.Section('ТВ', root, need_cancel=1, row=0)
internet_section = gearbot3.Section('Интернет', root, need_cancel=1, row=1)
software_rent_section = gearbot3.Section('Аренда ПО', root, need_cancel=1, row=1)
telephony_section = gearbot3.Section('Телефония', root, need_cancel=1, row=1)
hosting_section = gearbot3.Section('Хостинг', root, need_cancel=1, row=2)
check_balance_button = gearbot3.Intent('Проверить баланс', parent=root, out_text="Введите ваш телефонный номер без +7",
                                       row=2, this_id=0, final=root)

check_balance_button.writed = check_balance_func

write_to_us_button = gearbot3.Button('Написать нам', parent=root, on_click=feedback_func, row=2)
# test_checkboxgroup = gearbot3.CheckBoxGroup('Checkbox Test', parent=root, row=2)
# first_checkbox = gearbot3.CheckBox('FirstCheckBox', parent=test_checkboxgroup, row=0)
# second_checkbox = gearbot3.CheckBox('SecondCheckBox', parent=test_checkboxgroup, row=1)
# third_checkbox = gearbot3.CheckBox('ThirdCheckBox', parent=test_checkboxgroup, row=2)


# def showcheckboxresults(tb, chat_id, message_text):
#     for i in test_checkboxgroup.sub_buttons:
#         if issubclass(type(i), gearbot3.CheckBox):
#             print(i.text + " " + str(i.checked))


# final_checkbox = gearbot3.Button('ShowResult', parent=test_checkboxgroup, on_click=showcheckboxresults, row=3)

# "/" -> "Акции"

a_0 = gearbot3.TextButton('Утилиты', shares_section, '''Компьютер стал греться, тормозить и глючить? Не нужно вызывать мастера!
Возьми в аренду лицензионный антивирус Касперского и не беспокойся о вирусах!
[Подробнее по этой ссылке](http://telecom.kz/utilities)''', row=0)

a_1 = gearbot3.TextButton('Я люблю свою машину', shares_section, '''Смотри канал DTX в ID TV, проходи видео-квест и выйгрывай приз!
[Подробнее по этой ссылке](http://telecom.kz/dtx)''', row=1)

# "/" -> "Пакеты"

b_0 = gearbot3.Section('Интернет + TV + Unlim телефония', packages_section, need_cancel=1, row=0)

b_1 = gearbot3.TextButton('Пакет базовый', packages_section, '''Интернет + Unlim телефония
[Подробнее по этой ссылке](http://telecom.kz/services/packages/view/package-base2)''', row=0)

b_2 = gearbot3.TextButton('Универсальный номер', packages_section, '''Универсальный номер — это решение «три в одном»:
городской и мобильный номер внутри сети Алтел + мобильный интернет 4G
[Подробнее по этой ссылке](http://telecom.kz/services/packages/view/universal-number)''', row=1)

b_3 = gearbot3.TextButton('Семейное решение', packages_section, '''Счастье семьи в заботе о каждом!
[Подробнее об этой ссылке](http://telecom.kz/services/phone/view/family-mobile)''', row=1)

b_4 = gearbot3.Section('Дополнительные сервисы', packages_section, need_cancel=1, row=2)
