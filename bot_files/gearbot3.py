# _*_ coding: utf-8 _*_

"""
Created on 26.07.2016

:author: Semior001
Модуль для создания архитектуры клавиатур бота
"""

import shelve

import telebot
from telegram import ParseMode


# on_click(параметры) - должно вызываться при нажатии на САМУ КНОПКУ
# on_subbutton_click(параметры) - должно вызываться при нажатии ПОДКНОПКИ
#                                   например: Section, CheckBoxGroup

# Открытие хранилища для введенных данных


def open_intent():
    """Открывает файл intents.db"""
    global intents_storage
    intents_storage = shelve.open("Storage/intents.db", writeback=True)


def close_intent():
    """Закрывает файл intents.db"""
    intents_storage.close()


def open_checkboxes():
    """Открывает файл checkboxes.db"""
    global checkboxes_storage
    checkboxes_storage = shelve.open("Storage/checkboxes.db", writeback=True)


def close_checkboxes():
    """Закрывает файл checkboxes.db"""
    checkboxes_storage.close()


class Button:
    """Класс простой кнопки"""

    def __init__(self, text, parent, on_click=None, row=-1):
        """Инициализирует класс Button
        :param text: текст кнопки, который будет отображаться в родительской секции
        :param parent: ссылка на родительскую секцию
        :type parent: Section
        :param on_click: функция, вызываемая при нажатии на кнопку
        :param row: номер строки, в которой будет содержаться кнопка в родительской секции
        """
        self.text = text
        self.parent = parent
        if parent is not None:
            parent.add_button(self)
        self.on_click1 = on_click
        self.row = row

    def get_button(self):
        """Формирует кнопку для клавиатуры родительской секции
        :return: кнопка для родительской секции (telebot.types.KeyboardButton)
        """
        return telebot.types.KeyboardButton(self.text)

    def on_click(self, tb, chat_id, message_text):
        """функция, вызываемая при нажатии на кнопку
        :param tb:
        :param chat_id:
        :param message_text:
        """
        self.on_click1(tb, chat_id, message_text)
        self.parent.on_subbutton_click(tb, chat_id, message_text)


class TextButton(Button):
    """Класс кнопки, которая выводит текст при нажатии"""

    def __init__(self, text, parent, out_text, row=-1):
        """Инициализирует класс TextButton
        :param out_text: текст, выводимый при нажатии на кнопку секции в родительской секции
        """
        Button.__init__(self, text, parent, on_click=self.on_click, row=row)
        self.out_text = out_text

    def on_click(self, tb, chat_id, message_text):
        """Функция, вызываемая при нажатии на кнопку
        :param tb: объект сессии с telegram ботом
        :param chat_id: id обрабатываемого чата
        :param message_text: текст принятого сообщения
        """
        tb.send_message(chat_id, self.out_text, parse_mode=ParseMode.MARKDOWN)


class Section(Button):
    """ Класс секции """

    def __init__(self, text, parent=None, out_text="Выберите услугу:", need_cancel=0, row=-1):
        """Инициализирует класс Section
        :param out_text: текст, выводимый при нажатии на кнопку секции в родительской секции
        :param need_cancel: определяет, нужны ли кнопка "Назад" и "На главную" в клавиатуре данной секции
                            0 - не нужны
                            1 - нужна только кнопка "Назад"
                            2 - нужны кнопки "Назад" и "На главную"
        """
        Button.__init__(self, text, parent, self.on_click, row)
        self.out_text = out_text
        self.sub_buttons = []
        self.need_cancel = need_cancel

    def add_button(self, button):
        """Добавляет кнопку в клавиатуру данной секции
        :param button: кнопка
        :type button: Button
        """
        self.sub_buttons.append(button)

    def get_keyboard(self):
        """Формирует клавиатуру для отправки пользователю
        :return: клавиатура для отправки пользователю (telebot.types.ReplyKeyboardMarkup)
        """
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)

        n = 0
        current_row = 0
        while n < len(self.sub_buttons):
            row = []
            for i in self.sub_buttons:
                if i.row == current_row:
                    row.append(i.get_button())
                    n += 1
            current_row += 1
            markup.row(*row)

        if self.need_cancel == 1:
            markup.row(telebot.types.KeyboardButton('Назад'))
        elif self.need_cancel == 2:
            markup.row(telebot.types.KeyboardButton('Назад'), telebot.types.KeyboardButton('На главную'))

        return markup

    def on_click(self, tb, chat_id, message_text):
        """Вызывается при нажатии на кнопку
        :param tb: объект сессии с telegram ботом
        :param chat_id: id обрабатываемого чата
        :param message_text: текст принятого сообщения
        """
        tb.send_message(chat_id, self.out_text, reply_markup=self.get_keyboard(), parse_mode=ParseMode.MARKDOWN)

    def on_subbutton_click(self, tb, chat_id, message_text):
        """Данный метод сделан для того, чтобы PyCharm не ругался на отсутствие
        данного метода
        """
        pass


class Intent(Section):
    """Класс принимающий введенный текст"""

    def __init__(self, text, parent, out_text="Введите данные:",
                 need_cancel=1, row=-1, final=None, this_id=-1):
        """Инициализирует класс intent
        :param this_id: id этого intent'а
        :type this_id: int
        :param final: секция, в которую необходимо перейти после ввода данных
        :type final: Section
        """
        Section.__init__(self, text, parent=parent, out_text=out_text, need_cancel=need_cancel, row=row)
        if this_id == -1:
            raise TypeError("__init__() missing 1 required positional argument: 'this_id'")
        self.id = this_id
        self.final = final

    def set_final(self, final):
        """Задает секцию, в которую необходимо перейти после ввода данных
        :param final: секция, в которую необходимо перейти после ввода данных
        :type final: Section
        """
        self.final = final

    def writed(self, tb, chat_id, message_text):
        """Вызывается при вводе данных
        Если необходимо проверить введенный текст, надо переопределить этот метод
        и на возвращаемое значение вернуть 1, если введенные данные неправильные или
        0, если все введено верно
        :return: None
        """
        intents_storage[str(chat_id)][self.id] = message_text
        return None


class CheckBoxGroup(Intent):
    """Класс группы флажков"""

    def __init__(self, text, parent, out_text="Сделайте выбор:",
                 need_cancel=2, row=-1, final=None, this_id=-1):
        """Инициализирует класс checkbox'а
        :param this_id: id этого checkbox'а
        :type this_id: int
        :param final: секция, в которую необходимо перейти после ввода данных
        :type final: Section
        """
        Intent.__init__(self, text, parent, out_text, need_cancel, row, final, this_id)

    def set_final(self, final):
        """Задает секцию, в которую необходимо перейти после ввода данных
        :param final: секция, в которую необходимо перейти после ввода данных
        :type final: Section
        """
        self.final = final

    def on_subbutton_click(self, tb, chat_id, message_text):
        """
        Вызывается при нажатии на кнопку внутри этой группы
        :param tb: объект сессии с telegram ботом
        :param chat_id: id обрабатываемого чата
        :param message_text: текст принятого сообщения
        """
        tb.send_message(chat_id, self.out_text, reply_markup=self.get_keyboard(), parse_mode=ParseMode.MARKDOWN)

    def get_keyboard(self):
        """Формирует клавиатуру для отправки пользователю
        :return: клавиатура для отправки пользователю (telebot.types.ReplyKeyboardMarkup)
        """
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)

        n = 0
        current_row = 0
        while n < len(self.sub_buttons):
            row = []
            for i in self.sub_buttons:
                if i.row == current_row:
                    row.append(i.get_button())
                    n += 1
            current_row += 1
            markup.row(*row)

        if self.need_cancel == 1:
            markup.row(telebot.types.KeyboardButton('Назад'))
        elif self.need_cancel == 2:
            markup.row(telebot.types.KeyboardButton('Назад'), telebot.types.KeyboardButton('На главную'))
        return markup

